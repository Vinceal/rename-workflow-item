package com.stiltsoft.jira.rename.api;

public interface MyPluginComponent
{
    String getName();
}