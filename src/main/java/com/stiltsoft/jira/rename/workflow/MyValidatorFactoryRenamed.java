package com.stiltsoft.jira.rename.workflow;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;

import java.util.HashMap;
import java.util.Map;

public class MyValidatorFactoryRenamed extends AbstractWorkflowPluginFactory implements WorkflowPluginValidatorFactory {

    protected void getVelocityParamsForInput(Map<String, Object> map) {

    }

    protected void getVelocityParamsForEdit(Map<String, Object> map, AbstractDescriptor abstractDescriptor) {

    }

    protected void getVelocityParamsForView(Map<String, Object> map, AbstractDescriptor abstractDescriptor) {

    }

    public Map<String, ?> getDescriptorParams(Map<String, Object> map) {
        return new HashMap<String, Object>();
    }
}