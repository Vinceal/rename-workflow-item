package ut.com.stiltsoft.jira.rename;

import org.junit.Test;
import com.stiltsoft.jira.rename.api.MyPluginComponent;
import com.stiltsoft.jira.rename.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}